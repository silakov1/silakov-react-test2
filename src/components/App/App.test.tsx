import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('renders logo link', () => {
  render(<App />);
  const linkElement = screen.getByText(/logo/i);
  expect(linkElement).toBeInTheDocument();
});
