import {
  BrowserRouter,
  Route,
  Routes,
} from 'react-router-dom';
import Dashboard from '../Dashboard/Dashboard';
import Login from '../Login/Login';
import About from '../About/About';
import NotFound from '../NotFound/NotFound';
import Header from '../Header/Header';
import useToken from '../../useToken';
import Footer from '../Footer/Footer';
import './App.css';

function App() {
  const { token, setToken } = useToken();

  const handleLogout = () => {
    setToken('')
    localStorage.removeItem('token');
  }

  return (
    <div className="app">
      <BrowserRouter>
        <Header token={token} handleLogout={handleLogout} />
        <main className="app-main">
          <Routes>
            <Route path="/about" element={<About />} />
            <Route path="*" element={<NotFound />} />
            {!token ?
              <Route path="/" element={<Login setToken={setToken} />} />
              :
              <Route path="/" element={<Dashboard />} />
            }
          </Routes>
        </main>
        <Footer />
      </BrowserRouter>
    </div>
  );
}

export default App;
