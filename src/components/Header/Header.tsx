import { Link, NavLink, useNavigate, useLocation } from 'react-router-dom';
import './Header.css';

interface props {
  token: string;
  handleLogout: () => void;
}

export default function Header({ token, handleLogout }: props) {
  let navigate = useNavigate();
  let location = useLocation();

  const handleClickLogout = () => {
    navigate("/" + location.search)
    handleLogout()
  }

  return(
    <header className="header">
      <Link
        to="/"
        className="header__logo"
      >
        Logo_
      </Link>
      <nav>
        <ul className="header__nav">
          <li>
            <NavLink
              to="/about"
              className="header__link"
            >
              about
            </NavLink>
          </li>
          {!token ? 
            <li>
              <NavLink
                to="/"
                className="header__link"
              >
                login
              </NavLink>
            </li>
          :
            <li>
              <button
                onClick={() => handleClickLogout()}
                className="header__link"
              >
                logout
              </button>
            </li>
          }
        </ul>
      </nav>
    </header>
  );
}
