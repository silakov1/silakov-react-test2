import { SetStateAction, useState } from 'react';
import SearchBox from '../SearchBox/SearchBox';
import './Users.css';

interface props {
  users: {
    from_id: string;
    from_name: string;
    posts_count: number;
  }[];
  handleUserClick: (id: string) => void;
}

export default function Users({ users, handleUserClick }: props) {
  const [searchField, setSearchField] = useState<string>('');
  const [activeUserId, setActiveUserId] = useState<string>('');

  const handleClick = (id: string) => {
    handleUserClick(id);
    setActiveUserId(id);
  }

  const filteredUsers = users.filter(user => (
    user.from_name.toLowerCase().includes(searchField.toLocaleLowerCase())
  ));

  return(
    <div className="sidebar">
      <SearchBox
        placeholder="search user"
        handleChange={(e) => setSearchField(e.target.value)}
      />
      <ul className="user-list">
        {filteredUsers.map(user =>
          <li
            key={user.from_id}
            className="user-list__item"
          >
            <button
              className={`user-list__user ${activeUserId === user.from_id ? 'active' : ''}`}
              onClick={() => handleClick(user.from_id)}
            >
              <span>{user.from_name}</span>
              <span>({user.posts_count})</span>
            </button>
          </li>
        )}
      </ul>
    </div>
  );
}
