import React, { useEffect, useState } from 'react';
import useToken from '../../useToken';
import { getPosts } from '../../services/posts';
import UserPosts from './components/UserPosts/UserPosts';
import Users from './components/Users/Users';
import './Dashboard.css';

interface post {
  id: string;
  message: string;
  created_time: number;
  from_id: string;
  from_name: string;
  status: string;
}

interface user {
  from_id: string;
  from_name: string;
  posts_count: number;
}

interface userPost {
  id: string;
  message: string;
  created_time: number;
}

export default function Dashboard() {
  const { token } = useToken();
  const [posts, setPosts] = useState([]);
  const [users, setUsers] = useState<user[]>([]);
  const [userPosts, setUserPosts] = useState<userPost[]>([]);

  console.log('users', users)

  useEffect(() => {
    let mounted = true;
    getPosts(token)
    .then(response => {
      const filteredByUser = response
        .map((item: post) => item.from_name)
        .filter((value: any, index: any, self: string | any[]) => self.indexOf(value) === index)
        .sort((a: string, b: string) => (a > b) ? 1 : -1);

      const sortedUsers = filteredByUser.map((item: string) => {
        const count = response.filter((post: {from_name: string}) => post.from_name === item).length;
        const {from_name, from_id} = response.find((post: {from_name: string}) => post.from_name === item);
        const user = {
          from_name,
          from_id,
          posts_count: count,
        }
        return user
      })

      if(mounted) {
        setPosts(response);
        setUsers(sortedUsers);
      }
    });
    return () => {
      mounted = false;
    }
  }, [token])

  const handleUserClick = (id: string) => {
    const filteredPosts: React.SetStateAction<userPost[]> | userPost[] = [];
    posts.find((user: post) => {
      if (user.from_id === id) {
        filteredPosts.push({
          'id': user.id,
          'message': user.message,
          'created_time': user.created_time,
        })
      }
      return false
    })
    setUserPosts(filteredPosts);
  }

  return(
    <div className="dashboard">
      <Users users={users} handleUserClick={handleUserClick} />
      {userPosts.length !== 0 ?
        <UserPosts userPosts={userPosts} />
        :
        <div className="dashboard__content">no user selected</div>
      }
    </div>
  );
}
