export function getPosts(token: string) {
  const searchParams = new URLSearchParams({
    sl_token: token,
    page: '1'
  });
  const url = `https://api.supermetrics.com/assignment/posts?${searchParams.toString()}`;
  const options = {
    method: 'GET',
    headers: {}
  }
  return fetch(url, options)
    .then((response) => {
      if (!response.ok) {
        throw new Error('Something went wrong')
      }
      return response.json();
    })
    .then(
      response => {
        const res = response.data.posts;
        return res
      }
    )
    .catch(function(error) {
      console.log(error);
    });
}
